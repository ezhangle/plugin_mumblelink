#include <StdAfx.h>

#include <CPluginMumbleLink.h>
#include <Nodes/G2FlowBaseNode.h>
#include <Game.h>
#include <StringUtils.h>
#include <IViewSystem.h>
#include <IRenderAuxGeom.h>
#include <ICryAnimation.h>
#include <IAnimatedCharacter.h>


namespace MumbleLinkPlugin
{
	struct LinkedMem
	{
#ifdef WIN32
		UINT32  uiVersion;
		DWORD   uiTick;
#else
		uint32_t uiVersion;
		uint32_t uiTick;
#endif
		float   fAvatarPosition[3];
		float   fAvatarFront[3];
		float   fAvatarTop[3];
		wchar_t name[256];
		float   fCameraPosition[3];
		float   fCameraFront[3];
		float   fCameraTop[3];
		wchar_t identity[256];
#ifdef WIN32
		UINT32  context_len;
#else
		uint32_t context_len;
#endif
		unsigned char context[256];
		wchar_t description[2048];
	};


	class MumbleLinkNode : public CFlowBaseNode<eNCT_Instanced>
	{
		private:
			enum EInputPorts
			{
				EIP_ENABLE,
				EIP_DISABLE,
				EIP_CONTEXT,
				EIP_PLAYER_ID,
				EIP_BONE,
				EIP_DEBUG,
			};

			// Our associated entityID if we have one.
			LinkedMem* lm;


		public:

			MumbleLinkNode (SActivationInfo* pActInfo)
			{
				lm = NULL;
			}


			~MumbleLinkNode ()
			{
				// TODO: is this how we dispose of it or is more required?
				lm = NULL;
			}


			virtual void GetMemoryUsage (ICrySizer* s) const
			{
				s->Add (*this);
			}


			virtual IFlowNodePtr Clone (SActivationInfo* pActInfo)
			{
				return new MumbleLinkNode (pActInfo);
			}


			void InitMumble ()
			{

#ifdef WIN32
				HANDLE hMapObject = OpenFileMappingW (FILE_MAP_ALL_ACCESS, FALSE, L"MumbleLink");

				if (hMapObject == NULL)
				{
					return;
				}

				lm = (LinkedMem*) MapViewOfFile (hMapObject, FILE_MAP_ALL_ACCESS, 0, 0, sizeof (LinkedMem));

				if (lm == NULL)
				{
					CloseHandle (hMapObject);
					hMapObject = NULL;
					return;
				}

#else
				char memname[256];
				snprintf (memname, 256, "/MumbleLink.%d", getuid ());

				int shmfd = shm_open (memname, O_RDWR, S_IRUSR | S_IWUSR);

				if (shmfd < 0)
				{
					return;
				}

				lm = (LinkedMem*) (mmap (NULL, sizeof (struct LinkedMem), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0));

				if (lm == (void*) (-1))
				{
					lm = NULL;
					return;
				}

#endif
			}


			void UpdateMumble (SActivationInfo* pActInfo)
			{
				CRY_ASSERT (gEnv);
				CRY_ASSERT (gEnv->pSystem);
				CRY_ASSERT (gEnv->pGame->GetIGameFramework ());

				// Armour.
				if (!lm)
					return;

				// Tick tock.
				lm->uiTick++;

				if (lm->uiVersion != 2)
				{
					wcsncpy (lm->name, L"CryEngine 3.6.x", 256);
					wcsncpy (lm->description, L"CryEngine 3.6.x.", 2048);
					lm->uiVersion = 2;
				}

				bool allowDebug = GetPortBool (pActInfo, EIP_DEBUG);

				// Mumble uses a left handed coordinate system.
				// X positive towards "right".
				// Y positive towards "up".
				// Z positive towards "front".
				//
				// 1 unit = 1 meter

				// Retrieve the position of the player.
				Vec3 playerPosition;
				Quat playerRotation;

				// Get player.
				IActor* pActor = gEnv->pGame->GetIGameFramework ()->GetClientActor ();
				CRY_ASSERT (pActor);

				// Get Animated Character
				IAnimatedCharacter* pAnimCharacter = pActor->GetAnimatedCharacter ();
				ICharacterInstance* pCharacter = pActor->GetEntity ()->GetCharacter (0);

				// Get Skeleton Pose
				if (pAnimCharacter && pCharacter)
				{
					ISkeletonPose* pPose = pCharacter->GetISkeletonPose ();
					IDefaultSkeleton& targetSkeleton = pCharacter->GetIDefaultSkeleton ();

					if (pPose)
					{
						// The world position which acts as our camera target.
						int16 nBoneid = targetSkeleton.GetJointIDByName (GetPortString (pActInfo, EIP_BONE));
						if (nBoneid >= 0)
						{
							// Up and right directions are different when using a bone to the ones for a character's world position.
							int upColumn;
							int rightColumn;

							// If we can find the head bone, then we use that, otherwise default to just their basic location.
							if (nBoneid >= 0)
							{
								QuatT worldBone = pAnimCharacter->GetAnimLocation () * pPose->GetAbsJointByID (nBoneid);

								playerPosition = worldBone.t;
								playerRotation = worldBone.q;
								rightColumn = 0;
								upColumn = 2;
							}
							else
							{
								playerPosition = gEnv->pGame->GetIGameFramework ()->GetClientActor ()->GetEntity ()->GetWorldPos ();
								playerRotation = gEnv->pGame->GetIGameFramework ()->GetClientActor ()->GetEntity ()->GetWorldRotation ();
								upColumn = 2;
								rightColumn = 0;
							}

							// Position of the avatar.
							lm->fAvatarPosition [0] = playerPosition.x;
							lm->fAvatarPosition [1] = playerPosition.z;
							lm->fAvatarPosition [2] = playerPosition.y;

							// Unit vector pointing out of the avatars eyes (here Front looks into scene).
							lm->fAvatarFront [0] = playerRotation.GetColumn1 ().x;
							lm->fAvatarFront [1] = playerRotation.GetColumn1 ().z;
							lm->fAvatarFront [2] = playerRotation.GetColumn1 ().y;

							// Unit vector pointing out of the top of the avatars head (here Top looks straight up).
							lm->fAvatarTop [0] = playerRotation.GetColumn (upColumn).x;
							lm->fAvatarTop [1] = playerRotation.GetColumn (upColumn).z;
							lm->fAvatarTop [2] = playerRotation.GetColumn (upColumn).y;

							if (allowDebug)
							{
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawSphere (playerPosition, 0.14f, ColorB (255, 0, 0));
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawLine (playerPosition, ColorB (255, 0, 0), playerPosition + playerRotation.GetColumn (rightColumn), ColorB (255, 0, 0), 5.0f);
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawLine (playerPosition, ColorB (0, 255, 0), playerPosition + playerRotation.GetColumn1 (), ColorB (0, 255, 0), 5.0f);
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawLine (playerPosition, ColorB (0, 0, 255), playerPosition + playerRotation.GetColumn (upColumn), ColorB (0, 0, 255), 5.0f);
							}

							IView* presentView = gEnv->pGame->GetIGameFramework ()->GetIViewSystem ()->GetActiveView ();
							SViewParams viewParams = *(presentView->GetCurrentParams ());

							// Position of the camera.
							lm->fCameraPosition [0] = viewParams.position.x;
							lm->fCameraPosition [1] = viewParams.position.z;
							lm->fCameraPosition [2] = viewParams.position.y;

							// Unit vector pointing out of the camera (here Front looks into scene).
							lm->fCameraFront [0] = viewParams.rotation.GetColumn1 ().x;
							lm->fCameraFront [1] = viewParams.rotation.GetColumn1 ().z;
							lm->fCameraFront [2] = viewParams.rotation.GetColumn1 ().y;

							// Unit vector pointing out of the top of the camera (here Top looks straight up).
							lm->fCameraTop [0] = viewParams.rotation.GetColumn2 ().x;
							lm->fCameraTop [1] = viewParams.rotation.GetColumn2 ().z;
							lm->fCameraTop [2] = viewParams.rotation.GetColumn2 ().y;

							if (allowDebug)
							{
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawSphere (viewParams.position, 0.15f, ColorB (0, 127, 0));
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawLine (viewParams.position, ColorB (127, 0, 0), viewParams.position + viewParams.rotation.GetColumn0 (), ColorB (127, 0, 0), 5.0f);
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawLine (viewParams.position, ColorB (0, 127, 0), viewParams.position + viewParams.rotation.GetColumn1 (), ColorB (0, 127, 0), 5.0f);
								gEnv->pRenderer->GetIRenderAuxGeom ()->DrawLine (viewParams.position, ColorB (0, 0, 127), viewParams.position + viewParams.rotation.GetColumn2 (), ColorB (0, 0, 127), 5.0f);
							}
						}
					}
				}

				// Identifier which uniquely identifies a certain player in a context (e.g. the in-game Name).
				wstring playerID = CryStringUtils::UTF8ToWStr(GetPortString (pActInfo, EIP_PLAYER_ID));
				wcsncpy (lm->identity, playerID.data(), playerID.length());				

				// Context should be equal for players which should be able to hear each other positional and
				// differ for those who shouldn't
				// e.g. it could contain the [SERVER]:[PORT]/[TEAM]
				string context = GetPortString (pActInfo, EIP_CONTEXT);
				memcpy (lm->context, context.c_str(), context.length());
				lm->context_len = context.length();
			}


			virtual void GetConfiguration (SFlowNodeConfig& config)
			{
				static const SInputPortConfig inputs[] =
				{
					InputPortConfig_Void ("Enable",			_HELP ("Enable"), "Enable"),
					InputPortConfig_Void ("Disable",		_HELP ("Disable"), "Disable"),
					InputPortConfig<string> ("Context",		_HELP ("Equal for players who should be able to hear each other e.g. [SERVER][PORT][TEAM]"), "Context"),
					InputPortConfig<string> ("PlayerID",	_HELP ("A unique name that identifies the player within the given context."), "Player ID"),
					InputPortConfig<string> ("Bone",		_HELP ("This bone will be used for the player position and rotation. A value of 'Bip01 Camera' is usually best for bipeds."), "Bone"),
					InputPortConfig<bool> ("AllowDebug",	_HELP ("Allow debugging information to be displayed."), "Allow Debug"),

					InputPortConfig_Null (),
				};

				config.pInputPorts = inputs;
				config.pOutputPorts = NULL;
				config.sDescription = _HELP ("Passes player location information to a running Mumble client.");

				config.SetCategory (EFLN_APPROVED);
			}


			virtual void ProcessEvent (EFlowEvent evt, SActivationInfo* pActInfo)
			{
				switch (evt)
				{
					case eFE_Initialize:
						// Sent once after level has been loaded. It is NOT called on Serialization!
						OnInitialize (pActInfo);
						break;

					case eFE_Activate:
						// Sent if one or more input ports have been activated.
						OnActivate (pActInfo);
						break;

					case eFE_Update:
						// This event is called when the node is updated, typically each frame while it is in use.
						// You must tell the System that you want your node to be updated by using the
						// SetRegularlyUpdated function of the IFlowGraph object
						OnUpdate (pActInfo);
						break;

					case eFE_Suspend:
						OnSuspend (pActInfo);
						break;

					case eFE_Resume:
						OnResume (pActInfo);
						break;
				}
			}


			void OnInitialize (SActivationInfo* pActInfo)
			{
			}


			void OnActivate (SActivationInfo* pActInfo)
			{
				if (IsPortActive (pActInfo, EIP_ENABLE))
				{
					InitMumble ();
					pActInfo->pGraph->SetRegularlyUpdated (pActInfo->myID, true);
				}
				else if (IsPortActive (pActInfo, EIP_DISABLE))
				{
					lm = NULL;
					pActInfo->pGraph->SetRegularlyUpdated (pActInfo->myID, false);
				}
			}


			void OnUpdate (SActivationInfo* pActInfo)
			{
				UpdateMumble (pActInfo);
			}


			void OnSuspend (SActivationInfo* pActInfo)
			{
				pActInfo->pGraph->SetRegularlyUpdated (pActInfo->myID, false);
			}


			void OnResume (SActivationInfo* pActInfo)
			{
				pActInfo->pGraph->SetRegularlyUpdated (pActInfo->myID, true);
			}
	};
}

REGISTER_FLOW_NODE_EX ("Chrysalis:Mumble:Link", MumbleLinkPlugin::MumbleLinkNode, MumbleLinkNode);
