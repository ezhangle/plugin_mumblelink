/* MumbleLink_Plugin - for licensing and copyright see license.txt */

#include <IPluginBase.h>

#pragma once

/**
* @brief MumbleLink Plugin Namespace
*/
namespace MumbleLinkPlugin
{
	/**
	* @brief plugin MumbleLink concrete interface
	*/
	struct IPluginMumbleLink
	{
		/**
		* @brief Get Plugin base interface
		*/
		virtual PluginManager::IPluginBase* GetBase () = 0;

		// TODO: Add your concrete interface declaration here
	};
};