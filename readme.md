MumbleLink Plugin for CryEngine SDK
===================================

Alpha quality!

The purpose of the MumbleLink plugin is to take player positional information within a CryEngine 3.5.x game and
report this to a Mumble client.

The latest version of this plugin can be found at https://bitbucket.org/ivanhawkes/mumblelink

Installation / Integration
==========================

Currently you will need to compile the code, which will take care of installing the plugin to the correct folders.

Flownodes
=========

All functionality is contained within a single flownode called 'Chrysalis/Mumble/MumbleLink'.

To utilise the plugin features, place this node into your project and set up the ports appropriately.

Check the /examples folder for examples on how you might implement this flownode within your project.

* Enable - enable this flownode
* Disable - disable this flownode
* Context - a string representing a context within which the player exists. Typically you would break them down by things like
server, port, team or perhaps zone, region  or even via guild.
* PlayerID - a unique identifier for every player within this context.
* If this bone exists we will use it's position and rotation as the origin for the player. If not, the player's basic
position and rotation is used.
* AllowDebug - draws useful debug information onto the screen. This includes the location of the player, and their local right,
front and up vectors.

It should go without saying that you need to be running a copy of the Mumble client at the time you activate the 'Enable' port.

I seem to recall you also need to tick a box in Mumble to allow it to use the 3D positional co-ordinates. Go to the configuration
screen, and in the 'Plugins' section tick the box 'Link to Game and Transmit Position'.

Go to http://mumble.sourceforge.net/Positional-Audio to discover more.